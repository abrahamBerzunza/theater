package Main;

import Controller.LandingControl;
import View.LandingView;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Application {
    
    public static void main(String args[]) {
        try {
            
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            
            JOptionPane.showMessageDialog(null, e);
        }

        LandingView landingView = new LandingView();
        LandingControl landingControl = new LandingControl(landingView);
        landingControl.initComponents();
    }
}
