package Exceptions;

public class InvalidTextField extends RuntimeException
{
    public InvalidTextField(String message) 
    {
        super(message);
    }    
}
