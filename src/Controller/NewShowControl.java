package Controller;

import Database.DAOShow;
import Exceptions.InvalidTextField;
import Model.Show;
import View.AdministratorView;
import View.NewShowView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


public class NewShowControl implements ActionListener, ChangeListener
{
    private Show show;
    private NewShowView newShowView;
    
    public NewShowControl() {}
    
    public NewShowControl(Show show, NewShowView showView) 
    {
        this.show = show;
        this.newShowView = showView;
        
        this.newShowView.btnAdd.addActionListener(this);
        this.newShowView.btnReturn.addActionListener(this);
        
        this.newShowView.sliderDuration.addChangeListener(this);
    }
    
    //Initializer components in the GUI
    public void initComponents() 
    {
        newShowView.setTitle("Agregar obra");
        newShowView.setLocationRelativeTo(null);
        newShowView.setResizable(false);
        newShowView.setVisible(true);
        newShowView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        newShowView.txtDuration.setText(String.valueOf(newShowView.sliderDuration.getValue()));
    }
    
    //Save a show in the database
    public void saveShow() 
    {   
        String showName, description, actors;
        int duration;
        double cost;
        String nameManager, phone, email;
        
        try
        {
            if(newShowView.txtShowName.getText().isEmpty())
            {
                showName = null;
                throw new InvalidTextField("Porfavor ingrese un nombre de la obra");
            }
            else
            {
                showName = newShowView.txtShowName.getText();  
            }
            
            description = newShowView.txtDescription.getText();
            actors = newShowView.txtActors.getText();
            duration = newShowView.sliderDuration.getValue();
            cost = (double)newShowView.sppCost.getValue();
            nameManager = newShowView.txtNameManager.getText();
            phone = newShowView.txtPhone.getText();
            email = newShowView.txtEmail.getText();
            
            DAOShow daoShow = new DAOShow();
            
            daoShow.createShow( new Show(showName, description, actors, duration, cost, nameManager, phone, email) );
        }
        catch(InvalidTextField e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    
    //Method for cleaning the fields in the view
    public void restartInterface()
    {
        newShowView.txtShowName.setText(null); 
        newShowView.txtDescription.setText(null);
        newShowView.txtActors.setText(null);
        newShowView.txtNameManager.setText(null);
        newShowView.txtPhone.setText(null);
        newShowView.txtEmail.setText(null);
        newShowView.sppCost.setValue(0);
        newShowView.sliderDuration.setValue(60);
        newShowView.panelFields.setSelectedIndex(0);
    }
    
    //Method for controlling button actions
    @Override
    public void actionPerformed(ActionEvent e) 
    {      
        
        if(e.getSource() == newShowView.btnAdd)
        {
            saveShow(); 
            restartInterface();
        }

        if(e.getSource() == newShowView.btnReturn)
        {
            Show showModel = new Show();
            AdministratorView administratorView = new AdministratorView();
            AdministratorControl administratorControl = new AdministratorControl(showModel, administratorView);
            
            administratorControl.initComponents();
            
            newShowView.setVisible(false);
        }
    }
    
    //Method for checking the changes in the slider
    @Override
    public void stateChanged(ChangeEvent e)
    {
        if(e.getSource() == newShowView.sliderDuration)
        {
            newShowView.txtDuration.setText(String.valueOf(newShowView.sliderDuration.getValue()));
        }
    }
}
