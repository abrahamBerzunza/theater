package Controller;

import Controller.Commons.CommonsController;
import Database.DAOPerformance;
import Model.Show;
import View.AddPerformancesView;
import View.AdministratorView;
import View.CalendarView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class AddPerformancesControl extends CommonsController implements ActionListener {
    private Show show;
    private AddPerformancesView addPerformancesView;

    public AddPerformancesControl(Show show, AddPerformancesView addPerformancesView) 
    {
        this.show = show;
        this.addPerformancesView = addPerformancesView;
        
        this.addPerformancesView.btnReturn.addActionListener(this);
        this.addPerformancesView.btnAddPerformance.addActionListener(this);
        this.addPerformancesView.btnPerformancesCalendar.addActionListener(this);
    }
    
    public void initComponents()
    {
        addPerformancesView.setTitle("Agregar Funciones");
        addPerformancesView.txtDateChooser.setMinSelectableDate(super.setNextDate());
        addPerformancesView.txtShowName.setText(show.getShowName());
        addPerformancesView.setLocationRelativeTo(null);
        addPerformancesView.setResizable(false);
        addPerformancesView.setVisible(true);
        addPerformancesView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }
    
    public void saveSchedule()
    {
        int day, month, year; 
        int hourInit, minuteInit; 
        int hourFinal, minuteFinal; 
        String[] dateToken;
        int idShow;
        int response;
        
        try 
        {
            
            SimpleDateFormat dFormat = new SimpleDateFormat("dd-MM-YYYY");
            dateToken = dFormat.format(addPerformancesView.txtDateChooser.getDate()).split("-");
            
            day = Integer.valueOf(dateToken[0]);
            month = Integer.valueOf(dateToken[1]);
            year = Integer.valueOf(dateToken[2]);   
            hourInit = (int) addPerformancesView.sppHour.getValue();
            minuteInit = (int) addPerformancesView.sppMinute.getValue();
            hourFinal = hourInit + (show.getDuration() / 60);
            minuteFinal = minuteInit + (show.getDuration() % 60);
            idShow = show.getIdShow();
            
            DAOPerformance daoPerformance = new DAOPerformance();
            response = daoPerformance.createPerformance(day, month, year, hourInit, minuteInit, hourFinal, minuteFinal, idShow);
            
            if(response != 1) 
                JOptionPane.showMessageDialog(null, "Función agregada con éxito");
            
        }
        catch(NullPointerException e)
        {
            JOptionPane.showMessageDialog(null, "Por favor ingrese una fecha válida");
            System.out.println("Error " + e);
        } 

        
    }
    
    @Override
    public void actionPerformed(ActionEvent e) 
    {
        if(e.getSource() == addPerformancesView.btnAddPerformance)
        {
            saveSchedule();
        }
        
        if(e.getSource() == addPerformancesView.btnReturn)
        {
            Show showModel = new Show();
            AdministratorView administratorView = new AdministratorView();
            AdministratorControl administratorControl = new AdministratorControl(showModel, administratorView);
            
            administratorControl.initComponents();
            
            addPerformancesView.setVisible(false);
        }
        
        if(e.getSource() == addPerformancesView.btnPerformancesCalendar) {
            
            CalendarView calendarView = new CalendarView();
            CalendarControl calendarControl = new CalendarControl(calendarView);
            
            calendarControl.initComponents();
        }
    }
}
