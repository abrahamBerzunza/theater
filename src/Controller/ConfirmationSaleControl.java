package Controller;

import Controller.Commons.CommonsController;
import Database.DAOClient;
import Database.DAOPerformance;
import Database.DAOSeat;
import Database.DAOShow;
import Exceptions.InvalidTextField;
import Model.Client;
import Model.Performance;
import Model.Seat;
import Model.Show;
import Model.Ticket;
import View.ConfirmationSaleView;
import View.LandingView;
import View.SeattingsView;
import View.TicketView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ConfirmationSaleControl extends CommonsController implements ActionListener {
    
    private ConfirmationSaleView confirmationSaleView;
    private SeattingsView seattingsView;

    public ConfirmationSaleControl(ConfirmationSaleView confirmationSaleView, SeattingsView seattingsView) {
        
        this.confirmationSaleView = confirmationSaleView;
        this.seattingsView = seattingsView;
        
        this.confirmationSaleView.btnBuy.addActionListener(this);
        this.confirmationSaleView.btnCancel.addActionListener(this);
    }
    
    public void initComponents() {
        
        confirmationSaleView.setTitle("Confirmación de compra");
        confirmationSaleView.setResizable(false);
        confirmationSaleView.setLocationRelativeTo(null);
        confirmationSaleView.setVisible(true);
        confirmationSaleView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        fillInformationSale();
    }
    
    private void fillInformationSale() {
        
        DAOSeat daoSeat = new DAOSeat();
        DAOPerformance daoPerformance = new DAOPerformance();
        DAOShow daoShow = new DAOShow();
        ArrayList<Seat> seattings;
        Performance performance;
        Show show;
        double amount = 0;
        
        seattings = daoSeat.getAllSeattingsSelected();
        performance = daoPerformance.getPerformanceById(seattings.get(0).getIdPerformance());
        show = daoShow.getSingleShowById(performance.getIdShow());
        
        confirmationSaleView.lblShowName.setText(show.getShowName());
        confirmationSaleView.lblPerformance.setText(performance.toString());
        
        for (int i = 0; i < seattings.size(); i++) {
          
           confirmationSaleView.txtSeatting.append(String.valueOf(seattings.get(i).getNumberSeat()) + "\n");
           amount += seattings.get(i).getPriceByZone();
        }
         
        confirmationSaleView.lblAmount.setText(String.valueOf(amount));
        
    }
    
    private Client getClientInformation() {
        
        SimpleDateFormat dFormat = new SimpleDateFormat("dd-MM-YYYY hh:mm");
        
        String name = confirmationSaleView.txtClientName.getText();
        String lastName = confirmationSaleView.txtClientLastName.getText();
        String secondLastName = confirmationSaleView.txtClientSecondLastName.getText();
        double amount = Double.parseDouble(confirmationSaleView.lblAmount.getText());
        String date = dFormat.format(new Date());

        if (!name.isEmpty() && !lastName.isEmpty() && !secondLastName.isEmpty()) {

            return new Client(name, lastName, secondLastName, amount, date);
            
        } else {
            
            throw new InvalidTextField("Necesita llenar todos los campos");
        }
    }
    
    private void goToLandingView() {
        
        LandingView landingView = new LandingView();
        LandingControl landingControl = new LandingControl(landingView);
            
        landingControl.initComponents();
            
        confirmationSaleView.setVisible(false);
        seattingsView.setVisible(false);
        
    }
    
    private void generateTickets(int idClient) {
        Performance performance;
        int serie = 0;
        
        
        for (String seat : confirmationSaleView.txtSeatting.getText().split("\n")) {
            
            performance = super.divideDates(confirmationSaleView.lblPerformance.getText());
            
            String schedule = String.format("%02d:%02d", performance.getHourInitial(), performance.getMinuteInitial()) ;
            String date = String.format("%d/%d/%d", performance.getDay(), performance.getMonth(), performance.getYear());
            String folio = String.valueOf(idClient) + performance.getHourInitial() + performance.getMinuteInitial() + serie;
            String showName = confirmationSaleView.lblShowName.getText();
            
            TicketView ticketView = new TicketView();
            Ticket ticket = new Ticket(folio, showName, seat, schedule, date);
            TicketControl ticketControl = new TicketControl(ticketView, ticket);
            
            ticketControl.initComponents();
            
            serie++;
        }      
    }
    
    private void closeSale() {
        
        try {
            
            DAOClient daoClient = new DAOClient();
            DAOSeat daoSeat = new DAOSeat();
            int idClient;
            Client client;
        
            client = getClientInformation();
            daoClient.createClient(client);
        
            idClient = daoClient.getClientIdByName(client.getName(), client.getLastName(), client.getSecondLastName());
            daoSeat.setClientToSeatting(idClient);
           
            goToLandingView();
            generateTickets(idClient);
        }
        catch(InvalidTextField e) {
            
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == confirmationSaleView.btnBuy) {
            
            closeSale();
        }
        
        if(e.getSource() == confirmationSaleView.btnCancel) {
            
            confirmationSaleView.setVisible(false);
        }
    }
}
