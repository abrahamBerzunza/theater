package Controller;

import Controller.Commons.CommonsController;
import Database.DAOGeneral;
import Model.Performance;
import View.CalendarView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class CalendarControl extends CommonsController implements ActionListener{
    
    private CalendarView calendarView;
    
    DAOGeneral daoGeneral = new DAOGeneral();

    public CalendarControl(CalendarView performancesView) {
        this.calendarView = performancesView;
        
        this.calendarView.btnFilter.addActionListener(this);
        this.calendarView.btnDeleteFilter.addActionListener(this);
    }
    
    public void initComponents() {
        calendarView.setTitle("Lista de funciones");
        calendarView.setLocationRelativeTo(null);
        calendarView.setResizable(false);
        calendarView.setVisible(true);
        calendarView.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        showAllPerformances();
    }
    
    private void addShowsToTable(ArrayList<Performance> performanceList) {
        
        DefaultTableModel model = (DefaultTableModel) calendarView.calendarTable.getModel();
        Object[] table = new Object[model.getColumnCount()];
        
        //Variable important! for knowing the rows number in the table and then clear it
        super.numberRows = performanceList.size();

        for (int i = 0; i < performanceList.size(); i++) {
            
            table[0] = performanceList.get(i).getShowName();
            
            table[1] = String.format("%d/%d/%d", performanceList.get(i).getDay(), 
                                                 performanceList.get(i).getMonth(), 
                                                 performanceList.get(i).getYear());
            
            table[2] = String.format("%02d:%02d", performanceList.get(i).getHourInitial(), 
                                                  performanceList.get(i).getMinuteInitial());
            
            table[3] = String.format("%02d:%02d", performanceList.get(i).getHourFinal(), 
                                                  performanceList.get(i).getMinuteFinal());
            
            model.addRow(table);
        }
    }
    
    private void showAllPerformances() {
        
        ArrayList<Performance> performanceList;
        
        performanceList = daoGeneral.getAllShowsWithPerformances();
        addShowsToTable(performanceList);
    }
    
    private void filterPerformances() {
        
        ArrayList<Performance> performanceList;
        String[] dateFromToken, dateToToken;
        SimpleDateFormat dFormat = new SimpleDateFormat("dd-MM-YYYY");
        int fromYear, fromMonth, fromDay, toYear, toMonth, toDay;
        
        try {
            dateFromToken = dFormat.format(calendarView.txtDateFrom.getDate()).split("-");
            fromYear = Integer.valueOf(dateFromToken[2]);
            fromMonth = Integer.valueOf(dateFromToken[1]);
            fromDay = Integer.valueOf(dateFromToken[0]);

            dateToToken = dFormat.format(calendarView.txtDateTo.getDate()).split("-");
            toYear = Integer.valueOf(dateToToken[2]);
            toMonth = Integer.valueOf(dateToToken[1]);
            toDay = Integer.valueOf(dateToToken[0]); 
    
            System.out.println(fromYear + "/" + fromMonth + "/" + fromDay);
            System.out.println(toYear + "/" + toMonth + "/" + toDay);
            performanceList = daoGeneral.getShowsWithPerformancesFiltered(fromYear, fromMonth, fromDay, toYear, toMonth, toDay);
            addShowsToTable(performanceList);
        }
        catch(NullPointerException | ArrayIndexOutOfBoundsException e) {
            
            JOptionPane.showMessageDialog(null, "Por favor ingrese una fecha válida");
            showAllPerformances();
            System.out.println("Error " + e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == calendarView.btnFilter) {
            
            super.clearTable(calendarView.calendarTable);
            filterPerformances();
        }
        
        if(e.getSource() == calendarView.btnDeleteFilter) {
            
            super.clearTable(calendarView.calendarTable);
            showAllPerformances();
        }
    }
    
}
