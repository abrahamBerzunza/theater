package Controller;

import Model.Ticket;
import View.TicketView;
import javax.swing.JFrame;

public class TicketControl {
    
    private TicketView ticketView;
    private Ticket ticket;

    public TicketControl(TicketView ticketView, Ticket ticket) {
        this.ticketView = ticketView;
        this.ticket = ticket;
    }
    
    public void initComponents() {
        ticketView.setTitle("Boleto");
        ticketView.setResizable(false);
        ticketView.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        ticketView.setVisible(true);
        
        ticketView.lblFolio.setText(ticket.getFolio());
        ticketView.lblShow.setText(ticket.getShowName());
        ticketView.lblSeat.setText(ticket.getSeat());
        ticketView.lblSchedule.setText(ticket.getSchedule());
        ticketView.lblDate.setText(ticket.getDate());
    }
    
}
