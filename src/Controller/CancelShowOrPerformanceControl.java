package Controller;

import Controller.Commons.CommonsController;
import Database.DAOPerformance;
import Database.DAOShow;
import Model.Performance;
import Model.Show;
import View.CancelShowOrPerformanceView;
import View.AdministratorView;
import View.PaybackByCancelView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class CancelShowOrPerformanceControl extends CommonsController implements ActionListener {

    private Show show;
    private CancelShowOrPerformanceView cancelView;

    public CancelShowOrPerformanceControl(Show show, CancelShowOrPerformanceView cancelView) {
        this.show = show;
        this.cancelView = cancelView;

        this.cancelView.btnCancelPerformance.addActionListener(this);
        this.cancelView.btnCancelShow.addActionListener(this);
        this.cancelView.btnCancel.addActionListener(this);
    }

    public void initComponents() {
        
        cancelView.setTitle("Cancelar Obra");
        cancelView.txtShowName.setText(show.getShowName());
        cancelView.txtDescription.setText(show.getDescription());
        cancelView.setLocationRelativeTo(null);
        cancelView.setResizable(false);
        cancelView.setVisible(true);
        cancelView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        super.fillComboBoxPerformances(show.getIdShow(), cancelView.cbxPerformancesShow);
    }

    //Method for deleting performance switch user selection
    public void cancelPerformance() {
        
        Performance performance;
        int day, month, year, hour, minute;
        DAOPerformance daoPerformance = new DAOPerformance();
        
        //if you have shows with empty schedules use it, comment it or delete it if you hven't 
        if(cancelView.cbxPerformancesShow.getItemCount() != 0)
        {
             JOptionPane.showMessageDialog(null,"Se canceló la función " + cancelView.cbxPerformancesShow.getSelectedItem().toString() 
                 + " de la obra " + cancelView.txtShowName.getText());

            try
            {
                performance = super.divideDates(cancelView.cbxPerformancesShow.getSelectedItem());
                
                day = performance.getDay(); 
                month = performance.getMonth(); 
                year = performance.getYear(); 
                hour = performance.getHourInitial();
                minute = performance.getMinuteInitial();
                
                daoPerformance.deletePerformance(day, month, year, hour, minute);
                
                cancelView.cbxPerformancesShow.removeItem(cancelView.cbxPerformancesShow.getSelectedItem());
            } 
            catch (NumberFormatException e) 
            {
                 System.out.println("Error " + e);
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null,"Nada que eliminar");
           
        }
    }
    
    //Delete show and performances if the user select yes option.
    public void cancelShow() {
        
        int response;
        
        response = JOptionPane.showConfirmDialog(null, "¿Está seguro que desea eliminar esta obra con todas sus funciones?",
            "Alerta!" ,JOptionPane.YES_NO_OPTION);

        if (response == JOptionPane.YES_OPTION) {
            
            DAOShow daoShow = new DAOShow();

            try {
                
                daoShow.deleteShowById(show.getIdShow());
                goToAdministratorView();

            } catch (NumberFormatException e) {

                System.out.println("Error en cancelShow " + e);
            }
        }
    }

    public void goToAdministratorView() {
        
        Show showModel = new Show();
        AdministratorView landingView = new AdministratorView();
        AdministratorControl landingControl = new AdministratorControl(showModel, landingView);

        landingControl.initComponents();
            
        cancelView.setVisible(false);
    }
    
    private void openPaybackByCancel() {
        PaybackByCancelView paybackByCancelView = new PaybackByCancelView();
        PaybackByCancelControl paybackByCancelControl = new PaybackByCancelControl(paybackByCancelView);
        
        paybackByCancelControl.initComponents();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == cancelView.btnCancelPerformance) {
            
            cancelPerformance();
        }
        
        if(e.getSource() == cancelView.btnCancelShow) {
            
           cancelShow();
        }

        if (e.getSource() == cancelView.btnCancel) {
            
           goToAdministratorView(); 
           openPaybackByCancel();
        }

    }
}
