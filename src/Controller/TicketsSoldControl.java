package Controller;

import Controller.Commons.CommonsController;
import Database.DAOPerformance;
import Database.DAOSeat;
import Model.Performance;
import Model.Seat;
import Model.Show;
import View.TicketsSoldView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.table.DefaultTableModel;

public class TicketsSoldControl extends CommonsController implements ActionListener, ItemListener {
    
    private Show show;
    private TicketsSoldView ticketsSoldView;
    
    DAOSeat daoSeat = new DAOSeat();
    private int idPerformance;

    public TicketsSoldControl(Show show, TicketsSoldView ticketsView) {
        this.show = show;
        this.ticketsSoldView = ticketsView;
        
        this.ticketsSoldView.btnFilter.addActionListener(this);
        this.ticketsSoldView.btnDeleteFilters.addActionListener(this);
        
        this.ticketsSoldView.cbPerformances.addItemListener(this);
    }
    
    public void initComponents() {
        ticketsSoldView.setTitle("Boletos vendidos");
        ticketsSoldView.setLocationRelativeTo(null);
        ticketsSoldView.setResizable(false);
        ticketsSoldView.setVisible(true);
        ticketsSoldView.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        ticketsSoldView.lblShowName.setText(show.getShowName());
        super.fillComboBoxPerformances(show.getIdShow(), ticketsSoldView.cbPerformances);
    }
    
    private void fillSeattingsTable() {
        
        ArrayList<Seat> seatList;
        Performance performance;
        int day, month, year, hour, minute;
        DAOPerformance daoPerformance = new DAOPerformance();
        
        performance = divideDates(ticketsSoldView.cbPerformances.getSelectedItem());
        day = performance.getDay();
        month = performance.getMonth();
        year = performance.getYear();
        hour = performance.getHourInitial();
        minute = performance.getMinuteInitial();
        
        idPerformance = daoPerformance.getPerformanceByDate(day, month, year, hour, minute).getIdPerformance();

        DefaultTableModel model = (DefaultTableModel) ticketsSoldView.seattingsTable.getModel();
        Object[] table = new Object[model.getColumnCount()];

        seatList = daoSeat.getAllSeattingsOccuped(idPerformance);
        
        //value for counting objects and then clear the table. Initialize the global variable
        numberRows = seatList.size();

        for (int i = 0; i < seatList.size(); i++) {
            
            table[0] = seatList.get(i).getNumberSeat();
            table[1] = seatList.get(i).getZone();
           
            model.addRow(table);
        }
    }
    
    private void filterByZone() {
        
        ArrayList<Seat> seatList;
        String zone;
        
        zone = String.valueOf(ticketsSoldView.cbZone.getSelectedItem());
        seatList = daoSeat.getAllSeattingsOccupedByZone(idPerformance, zone);
        
        //value for counting objects and then clear the table. Initialize the global variable
        numberRows = seatList.size();
        
        DefaultTableModel model = (DefaultTableModel) ticketsSoldView.seattingsTable.getModel();
        Object[] table = new Object[model.getColumnCount()];
        
        for (int i = 0; i < seatList.size(); i++) {
            
            table[0] = seatList.get(i).getNumberSeat();
            table[1] = seatList.get(i).getZone();
           
            model.addRow(table);
        }
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == ticketsSoldView.btnFilter) {
            
            super.clearTable(ticketsSoldView.seattingsTable);
            filterByZone();
        }
        
        if(e.getSource() == ticketsSoldView.btnDeleteFilters) {
            
            super.clearTable(ticketsSoldView.seattingsTable);
            fillSeattingsTable();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
  
        if (e.getStateChange() == ItemEvent.SELECTED) {
            fillSeattingsTable();
        }

        if (e.getStateChange() == ItemEvent.DESELECTED) {
            super.clearTable(ticketsSoldView.seattingsTable);
        }
    }
}
