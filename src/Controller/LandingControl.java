package Controller;

import Controller.Commons.CommonsController;
import Database.DAOPerformance;
import Database.DAOShow;
import Model.Administrator;
import Model.Performance;
import Model.Seat;
import Model.Show;
import View.CashClosingView;
import View.LandingView;
import View.LoginView;
import View.SeattingsView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class LandingControl extends CommonsController implements ActionListener, ItemListener {

    private LandingView landingView;
    
    private Show show;

    public LandingControl(LandingView landingView) {
        
        this.landingView = landingView;

        this.landingView.btnMenuAdministrator.addActionListener(this);
        this.landingView.btnSelectSeatting.addActionListener(this);
        this.landingView.btnCashClosing.addActionListener(this);
        this.landingView.cbShows.addItemListener(this);
    }

    public void initComponents() {
        
        landingView.setTitle("Teatro - Patito Feo");
        landingView.setLocationRelativeTo(null);
        landingView.setResizable(false);
        landingView.setVisible(true);
        fillShowOptions();
    }

    //Find and get an arraylist with show's names from db and put them into the combobox
    private void fillShowOptions() {
        ArrayList<Show> showList;
        
        DAOShow daoShow = new DAOShow();
        showList = daoShow.getAllShows();
        
        for (int i = 0; i < showList.size(); i++) {
            landingView.cbShows.addItem(showList.get(i).getShowName());
        }
    }
    
    private void goToSeattingView() {
        
        DAOPerformance daoPerformance = new DAOPerformance();
        
        Performance performance = super.divideDates(landingView.cbPerformances.getSelectedItem());
        int day = performance.getDay();
        int month = performance.getMonth();
        int year = performance.getYear();
        int hour = performance.getHourInitial();
        int minute = performance.getMinuteInitial();
        
        double basePrice = show.getCost();
        int idPerformance = daoPerformance.getPerformanceByDate(day, month, year, hour, minute).getIdPerformance();
        
        Seat seattingModel = new Seat(basePrice, idPerformance);
        SeattingsView seattingView = new SeattingsView();
        SeattingsControl seattingsControl = new SeattingsControl(seattingView, seattingModel);

        seattingsControl.initComponents();

        landingView.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == landingView.btnMenuAdministrator) {
            
            Administrator administrator = new Administrator();
            LoginView loginView = new LoginView();
            LoginControl loginControl = new LoginControl(administrator, loginView);

            loginControl.initComponents();

            landingView.setVisible(false);
        }

        if (e.getSource() == landingView.btnSelectSeatting) {
            
            goToSeattingView();
        }
        
        if(e.getSource() == landingView.btnCashClosing) {
            
            CashClosingView cashClosingView = new CashClosingView();
            CashClosingControl cashClosingControl = new CashClosingControl(cashClosingView);
            
            cashClosingControl.initComponents();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            
            DAOShow daoShow = new DAOShow();
            
            show = daoShow.getSingleShowByName(String.valueOf(landingView.cbShows.getSelectedItem()));
            super.fillComboBoxPerformances(show.getIdShow(), landingView.cbPerformances);
        }
        
        if (e.getStateChange() == ItemEvent.DESELECTED) {
            
            landingView.cbPerformances.removeAllItems();
        }
    }

}
