package Controller;

import Database.DAOGeneral;
import View.CashClosingView;
import javax.swing.JFrame;

public class CashClosingControl {
    
    private CashClosingView cashClosingView;
    
    DAOGeneral daoGeneral = new DAOGeneral();

    public CashClosingControl(CashClosingView cashClosingView) {
        this.cashClosingView = cashClosingView;
    }
    
    public void initComponents() {
        
        cashClosingView.setTitle("Corte de Caja");
        cashClosingView.setLocationRelativeTo(null);
        cashClosingView.setResizable(false);
        cashClosingView.setVisible(true);
        cashClosingView.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        
        fillSeatSoldByZone();
        setTotalSeattingsSold();
        setTotalAmountSales();
    }
    
    private void fillSeatSoldByZone() {
        
        int gold, silver, copper, can, diamond;
        
        gold = daoGeneral.getSeattingsSoldByZone("Oro");
        silver = daoGeneral.getSeattingsSoldByZone("Plata");
        copper = daoGeneral.getSeattingsSoldByZone("Cobre");
        can = daoGeneral.getSeattingsSoldByZone("Lata");
        diamond = daoGeneral.getSeattingsSoldByZone("Diamante");
        
        cashClosingView.lblSeatCanSold.setText(String.valueOf(can));
        cashClosingView.lblSeatCopperSold.setText(String.valueOf(copper));
        cashClosingView.lblSeatDiamondSold.setText(String.valueOf(diamond));
        cashClosingView.lblSeatGoldSold.setText(String.valueOf(gold));
        cashClosingView.lblSeatSilverSold.setText(String.valueOf(silver));
    }
    
    private void setTotalSeattingsSold() {
 
        int seattings = daoGeneral.getTotalSeattingsSold();
        
        cashClosingView.lblTotalSeatSold.setText(String.valueOf(seattings));
    }
    
    private void setTotalAmountSales() {
        
        double amount;

        amount = daoGeneral.getTotalAmountSales();
        
        cashClosingView.lblTotalAmount.setText(String.valueOf(amount));
    }
}
