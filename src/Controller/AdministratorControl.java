package Controller;

import Database.DAOShow;
import Model.Show;
import View.AddPerformancesView;
import View.AdministratorView;
import View.CalendarView;
import View.CancelShowOrPerformanceView;
import View.EditShowView;
import View.LandingView;
import View.NewShowView;
import View.PaybackByCancelView;
import View.PaybackView;
import View.RescheduleShowView;
import View.TicketsSoldView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class AdministratorControl implements ActionListener {

    private Show show;
    private AdministratorView administratorView;

    public AdministratorControl(Show show, AdministratorView administratorView) {
        
        this.show = show;
        this.administratorView = administratorView;

        this.administratorView.btnAddShow.addActionListener(this);
        this.administratorView.btnEditShow.addActionListener(this);
        this.administratorView.btnAddPerformance.addActionListener(this);
        this.administratorView.btnCancelShowOrPerformance.addActionListener(this);
        this.administratorView.btnReschedulePerformance.addActionListener(this);
        this.administratorView.btnGoCalendar.addActionListener(this);
        this.administratorView.btnReturn.addActionListener(this);
        this.administratorView.btnSalesSeatting.addActionListener(this);
        this.administratorView.btnPayback.addActionListener(this);
        this.administratorView.btnPaybackByCancel.addActionListener(this);
    }

    public void initComponents() {
        
        administratorView.setTitle("Menú de Administrador");
        administratorView.setLocationRelativeTo(null);
        administratorView.setResizable(false);
        administratorView.setVisible(true);
        administratorView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        AddShowsToTable();
    }

    public void AddShowsToTable() {
        
        ArrayList<Show> listShows;
        DAOShow daoShow = new DAOShow();

        DefaultTableModel model = (DefaultTableModel) administratorView.tableShows.getModel();
        Object[] table = new Object[model.getColumnCount()];

        listShows = daoShow.getAllShows();

        for (int i = 0; i < listShows.size(); i++) {
            
            table[0] = listShows.get(i).getIdShow();
            table[1] = listShows.get(i).getShowName();
            table[2] = listShows.get(i).getDescription();
            table[3] = listShows.get(i).getDuration();
            
            model.addRow(table);
        }
    }
    
    //get the show that is selected in the moment for doing some action
    public Show getSelectedShow() {
        
        int numberRow;
        int idShow, duration;
        String nameShow, description;

        numberRow = administratorView.tableShows.getSelectedRow();

        if (numberRow != -1) {
            
            idShow = (int) administratorView.tableShows.getValueAt(numberRow, 0);
            nameShow = (String) administratorView.tableShows.getValueAt(numberRow, 1);
            description = (String) administratorView.tableShows.getValueAt(numberRow, 2);
            duration = (int) administratorView.tableShows.getValueAt(numberRow, 3);

            Show showModel = new Show(idShow, nameShow, description, duration);
            
            return showModel;
            
        } else {
            JOptionPane.showMessageDialog(null, "No ha seleccionado ningúna obra");
            return null;
        }
    }
    
    private void goToEditShowView() {
        
        Show showModel = getSelectedShow();
        
        if(showModel != null) {
            
            EditShowView editShowView = new EditShowView();
            EditShowControl editShowControl = new EditShowControl(editShowView, showModel);

            editShowControl.initComponents();

            administratorView.setVisible(false);
        }
    }

    private void goAddPerformancesView() {
        
        Show showModel = getSelectedShow();
        
        if(showModel != null) {
            
            AddPerformancesView addPerformanceView = new AddPerformancesView();
            AddPerformancesControl addPerformancesControl = new AddPerformancesControl(showModel, addPerformanceView);

            addPerformancesControl.initComponents();

            administratorView.setVisible(false);
        }
        
    }

    private void goCancelShowView() {
        
        Show showModel = getSelectedShow();

        if (showModel != null) {
            
            CancelShowOrPerformanceView cancelView = new CancelShowOrPerformanceView();
            CancelShowOrPerformanceControl cancelControl = new CancelShowOrPerformanceControl(showModel, cancelView);

            cancelControl.initComponents();

            administratorView.setVisible(false);
        }
            
    }
    
    private void goRescheduleView() {
        
        Show showModel = getSelectedShow();
        
        if(showModel != null) {
            
            RescheduleShowView rescheduleShowView = new RescheduleShowView();
            RescheduleControl rescheduleControl = new RescheduleControl(rescheduleShowView, showModel);

            rescheduleControl.initComponents();

            administratorView.setVisible(false);
        }
           
    }

    private void goTicketsView() {
        
        Show showModel = getSelectedShow();
        
        if(showModel != null) {
            
            TicketsSoldView ticketsView = new TicketsSoldView();
            TicketsSoldControl ticketsControl = new TicketsSoldControl(showModel, ticketsView);
            
            ticketsControl.initComponents();
        }         
    }
    
    private void goToPaybackView() {
        
        Show showModel = getSelectedShow();
        
        if(showModel != null) {
            
            PaybackView ticketsView = new PaybackView();
            PaybackControl paybackControl = new PaybackControl(showModel, ticketsView);
            
            paybackControl.initComponents();
            administratorView.setVisible(false);
        } 
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == administratorView.btnAddShow) {
            Show showModel = new Show();
            NewShowView newShowView = new NewShowView();
            NewShowControl newShowControl = new NewShowControl(showModel, newShowView);

            newShowControl.initComponents();

            administratorView.setVisible(false);
        }
        
        if(e.getSource() == administratorView.btnEditShow) {
            
            goToEditShowView();
        }

        if (e.getSource() == administratorView.btnAddPerformance) {
            
            goAddPerformancesView();
        }

        if (e.getSource() == administratorView.btnCancelShowOrPerformance) {
           
            goCancelShowView();
        }

        if (e.getSource() == administratorView.btnReschedulePerformance) {
            
            goRescheduleView();
        }

        if (e.getSource() == administratorView.btnReturn) {
            
            LandingView landingView = new LandingView();
            LandingControl landingControl = new LandingControl(landingView);
            landingControl.initComponents();
            administratorView.setVisible(false);
        }
        
        if(e.getSource() == administratorView.btnGoCalendar) {
            
            CalendarView calendarView = new CalendarView();
            CalendarControl calendarControl = new CalendarControl(calendarView);
            calendarControl.initComponents();

        }
        
        if(e.getSource() == administratorView.btnSalesSeatting) {
            
            goTicketsView();
        }
        
                
        if(e.getSource() == administratorView.btnPayback ) {
            
            goToPaybackView();
        }
        
        if(e.getSource() == administratorView.btnPaybackByCancel) {
            
            PaybackByCancelView paybackByCancelView = new PaybackByCancelView();
            PaybackByCancelControl paybackByCancelControl = new PaybackByCancelControl(paybackByCancelView);
            paybackByCancelControl.initComponents();
        }
    }
}
