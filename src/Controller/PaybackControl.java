package Controller;

import Controller.Commons.CommonsController;
import Database.DAOClient;
import Database.DAOPerformance;
import Database.DAOSeat;
import Model.Client;
import Model.Performance;
import Model.Seat;
import Model.Show;
import View.AdministratorView;
import View.PaybackView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class PaybackControl extends CommonsController implements ActionListener, ItemListener{
    
    private PaybackView paybackView;
    private Show show;

//    static ArrayList paybackList = new ArrayList();
    
    public PaybackControl(Show show, PaybackView paybackView) {
        this.paybackView = paybackView;
        this.show = show;
        
        this.paybackView.btnReturn.addActionListener(this);
        this.paybackView.btnCancelTicket.addActionListener(this);
        this.paybackView.cbPerformances.addItemListener(this);
        this.paybackView.cbZone.addItemListener(this);
    }
    
    public void initComponents() {

        paybackView.setTitle("Reembolsos");
        paybackView.setLocationRelativeTo(null);
        paybackView.setResizable(false);
        paybackView.setVisible(true);
        paybackView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        super.fillComboBoxPerformances(show.getIdShow(), paybackView.cbPerformances);
    }
    
    private void fillSeattingsTable() {
        
        ArrayList<Seat> seatList;
        Performance performance;
        String zone;
        Client client;
        int day, month, year, hour, minute, idPerformance;
        DAOSeat daoSeat = new DAOSeat();
        DAOPerformance daoPerformance = new DAOPerformance();
        DAOClient daoClient = new DAOClient();
        
        performance = divideDates(paybackView.cbPerformances.getSelectedItem());
        day = performance.getDay();
        month = performance.getMonth();
        year = performance.getYear();
        hour = performance.getHourInitial();
        minute = performance.getMinuteInitial();
        
        idPerformance = daoPerformance.getPerformanceByDate(day, month, year, hour, minute).getIdPerformance();

        DefaultTableModel model = (DefaultTableModel) paybackView.salesTable.getModel();
        Object[] table = new Object[model.getColumnCount()];

        zone = String.valueOf(paybackView.cbZone.getSelectedItem());
        seatList = daoSeat.getAllSeattingsOccupedByZone(idPerformance, zone);
        
        //value for counting objects and then clear the table. Initialize the global variable
        numberRows = seatList.size();

        for (int i = 0; i < seatList.size(); i++) {
            
            table[0] = seatList.get(i).getNumberSeat();
            table[1] = seatList.get(i).getZone();
            
            client = daoClient.getClientById(seatList.get(i).getIdClient());
            table[2] = client.getFullName();
            table[3] = client.getDate();
            
            model.addRow(table);
        }
    }
    
    private void Payback() throws ParseException 
    {
        int day, month, year, hour, minute;
        int numberRow;
        String saleDate;
        numberRow = paybackView.salesTable.getSelectedRow();
        Performance performance;
        if (numberRow != -1) {

           saleDate = (String) paybackView.cbPerformances.getSelectedItem();
           performance = divideDates(saleDate);
           
           day = performance.getDay();
           month = performance.getMonth();
           year = performance.getYear();
           hour = performance.getHourInitial();
           minute = performance.getMinuteInitial();
           
           String showDateFormat = day + "/" + month + "/" + year + " " + hour + ":" + minute;
            
           SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/YYYY hh:mm");
            
           Date showDate = dateFormat.parse(showDateFormat);
           
           Date currentDate = new Date(); 
            
           Calendar calendarCurrentDate = Calendar.getInstance();
           Calendar calendarSaleDate = Calendar.getInstance();
           
           calendarCurrentDate.setTime(currentDate);
           calendarSaleDate.setTime(showDate);

           if(isValidPayback(calendarCurrentDate,calendarSaleDate)) cancelTicket();
           
        } 
        else {
            
            JOptionPane.showMessageDialog(null, "No ha seleccionado un elemento"); 
        }
        
    }
    
    public boolean isValidPayback(Calendar calendarCurrentDate, Calendar calendarSaleDate) {
        
        int saleMonth = calendarSaleDate.get(Calendar.MONTH);
        int saleYear = calendarSaleDate.get(Calendar.YEAR);
        int numberRow = paybackView.salesTable.getSelectedRow();
        System.out.println(saleMonth + "-" + saleYear );
        
        long hours = cantidadTotalHoras(calendarCurrentDate, calendarSaleDate);
        long minutes = differenceMinutes(calendarCurrentDate, calendarSaleDate);

        if (minutes < 0) {
            
            System.out.println("Horas: " + (hours - 1) + " Minutos: " + (minutes + 60));
        } 
        else {
            
            System.out.println("Horas: " + (hours) + " Minutos : " + minutes);
        }

        if (hours > 48 && numberRow != -1) {
            
            return true;
        } 
        else {
            
            JOptionPane.showMessageDialog(null, "Se ha excedido el límite de 48 horas. No se puede efectuar el reembolso");
        }
       
        return false; 
    }
    
    private long cantidadTotalHoras(Calendar currentDate ,Calendar dateFromComboBox){

        long totalMinutos;
        totalMinutos=((dateFromComboBox.getTimeInMillis()- currentDate.getTimeInMillis())/1000/60/60);
        return totalMinutos;
    }
    
    private long differenceMinutes(Calendar currentDate, Calendar dateFromComboBox)
    {
        long diferenciaHoras;
       
        diferenciaHoras = (dateFromComboBox.get(Calendar.MINUTE) - currentDate.get(Calendar.MINUTE));
        
        return diferenciaHoras;
    }
    
    
    private void cancelTicket() {
        
        DAOSeat daoSeat = new DAOSeat();
        
        DAOPerformance daoPerformance = new DAOPerformance();
        int numberRow, day, month, year, hour, minute, idPerformance, idClient;
        Performance performance;
        String numberSeat;
        Seat seat;
        double amountPayback;
        
        numberRow = paybackView.salesTable.getSelectedRow();
        
        if(numberRow != -1) {
            
            numberSeat = (String) paybackView.salesTable.getValueAt(numberRow, 0);
        
            performance = divideDates(paybackView.cbPerformances.getSelectedItem());
            day = performance.getDay();
            month = performance.getMonth();
            year = performance.getYear();
            hour = performance.getHourInitial();
            minute = performance.getMinuteInitial();

            idPerformance = daoPerformance.getPerformanceByDate(day, month, year, hour, minute).getIdPerformance();
            seat = daoSeat.getSeattingByNumberSeat(numberSeat, idPerformance);
            amountPayback = seat.getPriceByZone();
            idClient = seat.getIdClient();
            
            discountAmount(amountPayback, idClient);
            liberateSeat(numberSeat, idPerformance);
            refreshTable();
        }
    }
    
    private void discountAmount(double amountPayback, int idClient) {
        
        DAOClient daoClient = new DAOClient();
        double newAmount;
        
        newAmount = daoClient.getClientById(idClient).getAmount() - amountPayback;
        
        daoClient.updateAmount(newAmount, idClient);
        JOptionPane.showMessageDialog(null, String.format("Monto de reembolso: $%.2f", amountPayback));
        
        if(daoClient.getClientById(idClient).getAmount() == 0)
            daoClient.deleteClientById(idClient);
    }
    
    private void liberateSeat(String numberSeat, int idPerformance) {
        DAOSeat daoSeat = new DAOSeat();
        daoSeat.deleteSeatByNumberSeat(numberSeat, idPerformance);
    }
    
    private void refreshTable() {
        super.clearTable(paybackView.salesTable);
        fillSeattingsTable();
    }
    
    public void goToAdministratorView() {
        
        Show showModel = new Show();
        AdministratorView landingView = new AdministratorView();
        AdministratorControl landingControl = new AdministratorControl(showModel, landingView);

        landingControl.initComponents();
            
        paybackView.setVisible(false);
    }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == paybackView.btnCancelTicket) {
            
            try {
                
                Payback();
            } 
            catch (ParseException ex) {
                
                JOptionPane.showMessageDialog(null, ex);
            }
        }
        
        if (e.getSource() == paybackView.btnReturn) {
            
            goToAdministratorView();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            
            fillSeattingsTable();
        }

        if (e.getStateChange() == ItemEvent.DESELECTED) {
            
            super.clearTable(paybackView.salesTable);
        }
    }
}
