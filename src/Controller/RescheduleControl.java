package Controller;

import Controller.Commons.CommonsController;
import Database.DAOPerformance;
import Database.DAOSeat;
import Model.Performance;
import Model.Show;
import View.AdministratorView;
import View.CalendarView;
import View.RescheduleShowView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class RescheduleControl extends CommonsController implements ActionListener {

    private RescheduleShowView rescheduleShowView;
    private Show show;
    
    private boolean performanceDeleted = false;

    public RescheduleControl(RescheduleShowView rescheduleShowView, Show show) {
        
        this.show = show;
        this.rescheduleShowView = rescheduleShowView;

        this.rescheduleShowView.btnSaveChanges.addActionListener(this);
        this.rescheduleShowView.btnReturn.addActionListener(this);
        this.rescheduleShowView.btnGoCalendar.addActionListener(this);
    }

    public void initComponents() {

        rescheduleShowView.setTitle("Reprogramar funciones");
        rescheduleShowView.txtShowName.setText(show.getShowName());
        rescheduleShowView.txtNewDate.setMinSelectableDate(super.setNextDate());
        rescheduleShowView.setLocationRelativeTo(null);
        rescheduleShowView.setResizable(false);
        rescheduleShowView.setVisible(true);
        rescheduleShowView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        super.fillComboBoxPerformances(show.getIdShow(), rescheduleShowView.cbPerformances);
    }
    
    //Delete performance switch user selection
    public void cancelPerformance() {
        
        Performance performance;
        int day, month, year, hour, minute;
        DAOPerformance daoPerformance = new DAOPerformance();
        
        try {
            performance = super.divideDates(rescheduleShowView.cbPerformances.getSelectedItem());
            day = performance.getDay(); 
            month = performance.getMonth() ; 
            year = performance.getYear(); 
            hour = performance.getHourInitial();
            minute = performance.getMinuteInitial();
                    
            daoPerformance.deletePerformance(day, month, year, hour, minute);
        } 
        catch (NumberFormatException e) {
            
            System.out.println("Error " + e);
        }
    }

    //Create a new Performance for replacing previous it
    public int createNewPerformance() {

        int day, month, year;
        int hourInit, minuteInit;
        int hourFinal, minuteFinal;
        int idShow;
        int response = 0;
        String[] dateToken;

        try {

            SimpleDateFormat dFormat = new SimpleDateFormat("dd-MM-YYYY");
            dateToken = dFormat.format(rescheduleShowView.txtNewDate.getDate()).split("-");

            day = Integer.valueOf(dateToken[0]);
            month = Integer.valueOf(dateToken[1]);
            year = Integer.valueOf(dateToken[2]);
            hourInit = (int) rescheduleShowView.spHour.getValue();
            minuteInit = (int) rescheduleShowView.spMinutes.getValue();
            hourFinal = hourInit + (show.getDuration() / 60);
            minuteFinal = minuteInit + (show.getDuration() % 60);
            idShow = show.getIdShow();

            DAOPerformance daoPerformance = new DAOPerformance();
            response = daoPerformance.createPerformance(day, month, year, hourInit, 
                                             minuteInit, hourFinal, minuteFinal,
                                             idShow);
            
            if(response != 1) updateSeattingsReference(day, month, year, hourInit, minuteInit);

        } catch (NullPointerException e) {
            
            JOptionPane.showMessageDialog(null, "Por favor ingrese una fecha válida");
            System.out.println("Error " + e);
            
        }
        
        return response;
    }
    
    private void updateSeattingsReference(int day, int month, int year, int hourInit, int minuteInit) {
        
        int idPerformance;
        Performance performance;
        DAOPerformance daoPerformance = new DAOPerformance();
        DAOSeat daoSeat = new DAOSeat();
        
        performance = daoPerformance.getPerformanceByDate(day, month, year, hourInit, minuteInit);
        idPerformance = performance.getIdPerformance();
        
        daoSeat.setPerformanceToSeatting(idPerformance);
    }
    
    //Do the operations for reschedule the performance
    public void reschedulePerformance() {
        //Validation if the show dont have performances for rescheduling
        if(rescheduleShowView.cbPerformances.getItemCount() != 0) {
           
            if(!performanceDeleted) {
                
                cancelPerformance();
                
                if(createNewPerformance() != 1) {
                    
                    JOptionPane.showMessageDialog(null, "Función reprogramada");
                    rescheduleShowView.cbPerformances.removeAllItems();
                    initComponents();
                }
                else{
                    
                  rescheduleShowView.cbPerformances.setEnabled(false);
                  rescheduleShowView.btnReturn.setEnabled(false);
                  performanceDeleted = true;  
                }
            }
            else {

                if(createNewPerformance() != 1) {
                    JOptionPane.showMessageDialog(null, "Función reprogramada");
                    rescheduleShowView.cbPerformances.removeAllItems();
                    rescheduleShowView.cbPerformances.setEnabled(true);
                    rescheduleShowView.btnReturn.setEnabled(true);
                    initComponents();
                    performanceDeleted = false; 
                }
            }
        }
        else {
            
            JOptionPane.showMessageDialog(null,"Necesita tener una funciones para reprogramar");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
       
        if (e.getSource() == rescheduleShowView.btnSaveChanges) {
            
            reschedulePerformance();
        }
        
        if (e.getSource() == rescheduleShowView.btnReturn) {
            
            Show showModel = new Show();
            AdministratorView administratorView = new AdministratorView();
            AdministratorControl administratorControl = new AdministratorControl(showModel, administratorView);

            administratorControl.initComponents();

            rescheduleShowView.setVisible(false);
        }
        
        if(e.getSource() == rescheduleShowView.btnGoCalendar) {
            
            CalendarView calendarView = new CalendarView();
            CalendarControl calendarControl = new CalendarControl(calendarView);
            calendarControl.initComponents();
        }

    }

}
