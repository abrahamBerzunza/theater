package Controller;

import Database.DAOShow;
import Exceptions.InvalidTextField;
import Model.Show;
import View.AdministratorView;
import View.EditShowView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class EditShowControl implements ActionListener, ChangeListener {
    
    private EditShowView editShowView;
    private Show show;
    
    DAOShow daoShow = new DAOShow();

    public EditShowControl(EditShowView editShowView, Show show) {
        this.editShowView = editShowView;
        this.show = show;
        
        this.editShowView.btnReturn.addActionListener(this);
        this.editShowView.btnUpdate.addActionListener(this);
        
        this.editShowView.sliderDuration.addChangeListener(this);
    }
    
    public void initComponents() {
        editShowView.setTitle("Editar obra");
        editShowView.setLocationRelativeTo(null);
        editShowView.setResizable(false);
        editShowView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        editShowView.setVisible(true);
        
        fillFieldsWithShowInformation();
    }
    
        
    private void fillFieldsWithShowInformation() {
        
        Show editShow;
        
        editShow = daoShow.getSingleShowById(show.getIdShow());
        
        editShowView.txtShowName.setText(editShow.getShowName());
        editShowView.txtDescription.setText(editShow.getDescription());
        editShowView.txtActors.setText(editShow.getActors());
        editShowView.sppCost.setValue(editShow.getCost());
        editShowView.txtDuration.setText(String.valueOf(editShow.getDuration()));
        editShowView.txtNameManager.setText(editShow.getNameManager());
        editShowView.txtPhone.setText(editShow.getPhone());
        editShowView.txtEmail.setText(editShow.getEmail());
    }
    
    private void updateShow() {
        
        String showName, description, actors;
        int duration;
        double cost;
        String nameManager, phone, email;
        
        try
        {
            if(editShowView.txtShowName.getText().isEmpty())
            {
                showName = null;
                throw new InvalidTextField("Porfavor ingrese un nombre de la obra");
            }
            else
            {
                showName = editShowView.txtShowName.getText();  
            }
            
            description = editShowView.txtDescription.getText();
            actors = editShowView.txtActors.getText();
            duration = editShowView.sliderDuration.getValue();
            cost = (double)editShowView.sppCost.getValue();
            nameManager = editShowView.txtNameManager.getText();
            phone = editShowView.txtPhone.getText();
            email = editShowView.txtEmail.getText();
            
            daoShow.updateShowById( new Show(showName, description, actors, duration, cost, nameManager, phone, email), show.getIdShow());
        }
        catch(InvalidTextField e)
        {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == editShowView.btnReturn) {
            
            Show showModel = new Show();
            AdministratorView administratorView = new AdministratorView();
            AdministratorControl administratorControl = new AdministratorControl(showModel, administratorView);
            
            administratorControl.initComponents();
            
            editShowView.setVisible(false);
        }
        
        if(e.getSource() == editShowView.btnUpdate) {
            
            updateShow();
        }
    }

    @Override
    public void stateChanged(ChangeEvent e) {
        
        if(e.getSource() == editShowView.sliderDuration)
        {
            editShowView.txtDuration.setText(String.valueOf(editShowView.sliderDuration.getValue()));
        }
    }
}
