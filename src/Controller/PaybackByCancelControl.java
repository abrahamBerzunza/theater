package Controller;

import Controller.Commons.CommonsController;
import Database.DAOClient;
import Database.DAOSeat;
import Model.Client;
import View.PaybackByCancelView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class PaybackByCancelControl extends CommonsController implements ActionListener {
    
    private PaybackByCancelView paybackCancelView;
    
    DAOSeat daoSeat = new DAOSeat();
    DAOClient daoClient = new DAOClient();

    public PaybackByCancelControl(PaybackByCancelView paybackCancelView) {
        this.paybackCancelView = paybackCancelView;
        
        this.paybackCancelView.btnPayback.addActionListener(this);
        this.paybackCancelView.btnClose.addActionListener(this);
    }
    
    public void initComponents() {
        
        paybackCancelView.setTitle("Reembolsos por cancelación");
        paybackCancelView.setLocationRelativeTo(null);
        paybackCancelView.setResizable(false);
        paybackCancelView.setVisible(true);
        paybackCancelView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        clientsToPaybackMoney();
    }
    
    //Fill table with a client list to have to payback money
    private void clientsToPaybackMoney() {
        
        ArrayList idClientsList;

        DefaultTableModel model = (DefaultTableModel) paybackCancelView.tableClientPayback.getModel();
        Object[] table = new Object[model.getColumnCount()];

        idClientsList = daoSeat.getSeattingsCanceled();

        //Initialization necesary for count rows number and then clean the table
        super.numberRows = idClientsList.size(); 
        
        for (int i = 0; i < idClientsList.size(); i++) {
            
            int idClient = Integer.valueOf(idClientsList.get(i).toString());
            
            Client client = daoClient.getClientById(idClient);
            
            table[0] = client.getIdClient();
            table[1] = client.getName();
            table[2] = client.getLastName();
            table[3] = client.getAmount();
            
            model.addRow(table);
        }
    }
    
    private void payback() {
        
        int numberRow;
        
        numberRow = paybackCancelView.tableClientPayback.getSelectedRow();
        
        if(numberRow != -1) {
            
            int idClient = (int) paybackCancelView.tableClientPayback.getValueAt(numberRow, 0);
            double amount = (double) paybackCancelView.tableClientPayback.getValueAt(numberRow, 3);
            
            daoClient.deleteClientById(idClient);
            daoSeat.deleteSelectedSeattings();
            
            JOptionPane.showMessageDialog(null, "Se ha reembolsado: $" + amount);
            
        }
        else {
            
            JOptionPane.showMessageDialog(null, "No ha seleccionado ningúna cliente");
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == paybackCancelView.btnPayback) {
            
            payback();
            super.clearTable(paybackCancelView.tableClientPayback);
            clientsToPaybackMoney();
        }
        
        if(e.getSource() == paybackCancelView.btnClose) {
            
            paybackCancelView.setVisible(false);
        }
    }
}
