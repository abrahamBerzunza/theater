package Controller.Commons;

public class SizeController {
    
    public final static int POSITION = 40;
    public final static int PADDING = 35;
    public final static int HEIGHT = 40;
    public final static int WIDTH = 40;
    
    private int row;
    private int limitRow;
    private int column;
    private int limitColumn;

    public SizeController(int row, int limitRow, int column, int limitColumn) {
        this.row = row;
        this.limitRow = limitRow;
        this.column = column;
        this.limitColumn = limitColumn;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getLimitRow() {
        return limitRow;
    }

    public void setLimitRow(int limitRow) {
        this.limitRow = limitRow;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getLimitColumn() {
        return limitColumn;
    }

    public void setLimitColumn(int limitColumn) {
        this.limitColumn = limitColumn;
    }
    
    
}
