package Controller.Commons;

import Database.DAOPerformance;
import Model.Performance;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class CommonsController {
    
    public int numberRows;
    
    public Date setNextDate() 
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date()); 
        calendar.add(Calendar.DAY_OF_YEAR, 1);  
        
        return calendar.getTime(); 
    }
    
    //Get datas from comboBox (divide format)
    public Performance divideDates(Object item)
    {
        String[] dateToken;
        String[] auxiliarToken;
        String[] hourToken;
        String[] hourpartToken;
        int day, month, year, hour,minute;
        
        auxiliarToken = String.valueOf(item).split(" de ");
        dateToken = String.valueOf(auxiliarToken[0]).split("/");
        day = Integer.valueOf(dateToken[0]);
        month = Integer.valueOf(dateToken[1]);
        year = Integer.valueOf(dateToken[2]);
        
        hourToken = String.valueOf(auxiliarToken[1]).split(" a ");
        hourpartToken = String.valueOf(hourToken[0]).split(":");
        hour = Integer.valueOf(hourpartToken[0]);
        minute = Integer.valueOf(hourpartToken[1]);

        Performance performance = new Performance(day, month, year, hour, minute);
        
        return performance;
 
    }
    
    /*
        Get all performances by show
        format the date and put into the combobox
    */
    public void fillComboBoxPerformances(int idShow, JComboBox combobox) {
        
        DAOPerformance daoPerformance = new DAOPerformance();
        ArrayList<Performance> listPerformance;
        int day, month, year, hourInit, minuteInit, hourFinal, minuteFinal;
        String dateShow, hourInitShow, hourFinalShow;
        
        listPerformance = daoPerformance.getPerformancesByShowId(idShow);

        for (int i = 0; i < listPerformance.size(); i++) {

            if (listPerformance.get(i).isAvaible()) {

                day = listPerformance.get(i).getDay();
                month = listPerformance.get(i).getMonth();
                year = listPerformance.get(i).getYear();
                hourInit = listPerformance.get(i).getHourInitial();
                minuteInit = listPerformance.get(i).getMinuteInitial();
                hourFinal = listPerformance.get(i).getHourFinal();
                minuteFinal = listPerformance.get(i).getMinuteFinal();
                hourInitShow = String.format("%02d:%02d", hourInit, minuteInit);
                hourFinalShow = String.format("%02d:%02d", hourFinal, minuteFinal);
                
                dateShow = day + "/" + month + "/" + year + " de " + hourInitShow + " a " + hourFinalShow;
                combobox.addItem(dateShow);
            }
        }
    }
    
    //Method for clear tableObj
    public void clearTable(JTable table) {
       
       DefaultTableModel model = (DefaultTableModel)table.getModel();
       
       for (int i = 0; i < numberRows; i++) {
           
            model.removeRow(0);
       }
    }
}
