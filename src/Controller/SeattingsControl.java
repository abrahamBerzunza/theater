package Controller;

import Controller.Commons.SizeController;
import static Controller.Commons.SizeController.HEIGHT;
import static Controller.Commons.SizeController.PADDING;
import static Controller.Commons.SizeController.POSITION;
import static Controller.Commons.SizeController.WIDTH;
import Database.DAOSeat;
import Model.Seat;
import View.ConfirmationSaleView;
import View.LandingView;
import View.SeattingsView;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class SeattingsControl implements ActionListener {
    
    private SeattingsView seattingView;
    private Seat seattingModel;
    
    public static final Color LIGHT_BLUE = new Color(102,204,255);
    public final static int ROWS = 9;
    public final static int COLUMNS = 21;
    
    private Seat[][] seattings = new Seat[ROWS][COLUMNS];
    
    //Create size for each zone
    SizeController silverSizeLeft = new SizeController(1, 3, 1, 7);
    SizeController silverSizeRight = new SizeController(1, 3, 15, 21);
    SizeController canSizeLeft = new SizeController(3, 9, 1, 7);
    SizeController canSizeRight = new SizeController(3, 9, 15, 21);
    SizeController goldSize = new SizeController(1, 3, 7, 15);
    SizeController diamondSize = new SizeController(3, 6, 7, 15);
    SizeController copperSize = new SizeController(6, 9, 7, 15);

    public SeattingsControl(SeattingsView seattingView, Seat seatting) {
        this.seattingView = seattingView;
        this.seattingModel = seatting;
        
        this.seattingView.btnCancel.addActionListener(this);
        this.seattingView.btnConfirm.addActionListener(this);
    }
    
    public void initComponents() {
        seattingView.setTitle("Seleccionar asientos");
        seattingView.setLocationRelativeTo(null);
        seattingView.setResizable(false);
        seattingView.setVisible(true);
        seattingView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        
        buildSeattings(silverSizeLeft, "Plata", LIGHT_BLUE, 0.75);
        buildSeattings(silverSizeRight, "Plata", LIGHT_BLUE, 0.75);
        buildSeattings(canSizeLeft, "Lata", Color.LIGHT_GRAY, 0.5);
        buildSeattings(canSizeRight, "Lata", Color.LIGHT_GRAY, 0.5);
        buildSeattings(goldSize, "Oro", Color.YELLOW, 0.9);
        buildSeattings(diamondSize, "Diamante", Color.GREEN, 1);
        buildSeattings(copperSize, "Cobre", Color.ORANGE, 0.6);
        
        markOccupedSeattings();
    }
    
    private void buildSeattings(SizeController size, String zone, Color color, double percent) {
        
        for (int i = size.getRow(); i < size.getLimitRow(); i++) {
            
            for (int j = size.getColumn(); j < size.getLimitColumn(); j++) {
                
                double priceByZone = seattingModel.getBasePrice() * percent;
                int idPerformance = seattingModel.getIdPerformance();
                int idClient = seattingModel.getIdClient();
                
                seattings[i][j] = new Seat(i, j, zone, priceByZone, idPerformance,idClient);
                seattings[i][j].setBounds((POSITION * j - PADDING), (POSITION * i - PADDING), WIDTH, HEIGHT);
                seattings[i][j].setBackground(color);
                seattingView.panelSeattings.add(seattings[i][j]);
                
                switch(i) {
                    
                    case 1:
                        seattings[i][j].setText("H" + j);
                        break;
                        
                    case 2:
                        seattings[i][j].setText("G" + j);
                        break;
                        
                    case 3:
                        seattings[i][j].setText("F" + j);
                        break;
                        
                    case 4:
                        seattings[i][j].setText("E" + j);
                        break;
                        
                    case 5:
                        seattings[i][j].setText("D" + j);
                        break;
                        
                    case 6:
                        seattings[i][j].setText("C" + j);
                        break;
                        
                    case 7:
                        seattings[i][j].setText("B" + j);
                        break;
                        
                    case 8: 
                        seattings[i][j].setText("A" + j);
                        break;
                }
            }
        }
    }
    
    private boolean userSelected() {
        
        for (int i = 1; i < ROWS; i++) {
            for (int j = 1; j < COLUMNS; j++) {
                
                if(seattings[i][j].getBackground() == Color.RED) return true;
            }
        }
        
        return false;
    }
    
    private void markOccupedSeattings() {
        DAOSeat daoSeat = new DAOSeat();
        ArrayList<Seat> seattingsOccuped;
        
        seattingsOccuped = daoSeat.getAllSeattingsOccuped(seattingModel.getIdPerformance());
        
        for (int i = 0; i < seattingsOccuped.size(); i++) {
            
            int row = seattingsOccuped.get(i).getRow();
            int column = seattingsOccuped.get(i).getColumn();
            
            seattings[row][column].setBackground(Color.MAGENTA);
            seattings[row][column].setEnabled(false);
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if(e.getSource() == seattingView.btnConfirm) {
            
            if(userSelected()) {
               ConfirmationSaleView confirmationSaleView = new ConfirmationSaleView();
               ConfirmationSaleControl confirmationSaleControl = new ConfirmationSaleControl(confirmationSaleView, seattingView);
            
               confirmationSaleControl.initComponents(); 
            }
            else {
                
                JOptionPane.showMessageDialog(null, "No tiene asientos seleccionados");
            }
            
        }
        
        if(e.getSource() == seattingView.btnCancel) {
            
            LandingView landingView = new LandingView();
            LandingControl landingControl = new LandingControl(landingView);
            landingControl.initComponents();
            seattingView.setVisible(false);
            
            DAOSeat daoSeat = new DAOSeat();
            daoSeat.deleteSelectedSeattings();
        }
    }
}
