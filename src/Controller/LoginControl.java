package Controller;

import Database.DAOAdministrator;
import Model.Administrator;
import Model.Show;
import View.AdministratorView;
import View.LandingView;
import View.LoginView;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class LoginControl implements ActionListener {

    private Administrator administrator;
    private LoginView loginView;

    public LoginControl(Administrator administrator, LoginView loginView) {
        this.administrator = administrator;
        this.loginView = loginView;

        this.loginView.btnLogin.addActionListener(this);
        this.loginView.btnCancel.addActionListener(this);
    }

    public void initComponents() {
        loginView.setTitle(" Teatro \"Patito Feo\" ");
        loginView.setLocationRelativeTo(null);
        loginView.setResizable(false);
        loginView.setVisible(true);
        loginView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }

    public void login() {
        ArrayList<Administrator> administrators;
        String username, password;

        DAOAdministrator daoAdministrator = new DAOAdministrator();
        administrators = daoAdministrator.getAdministrators();

        username = loginView.txtUsername.getText();
        password = administrator.HashPassword(String.valueOf(loginView.txtPassword.getPassword()));

        if (userExist(administrators, username, password)) {
            Show showModel = new Show();
            AdministratorView administratorView = new AdministratorView();
            AdministratorControl administratorControl = new AdministratorControl(showModel, administratorView);

            administratorControl.initComponents();

            loginView.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(null, "Usuario o contraseña incorrectos");
        }
    }

    //Find and check the username and password; also get user's name and lastname & divides them into two strings
    public boolean userExist(ArrayList<Administrator> administrators, String username, String password) {
        String name, lastname;
        boolean founded = false;

        for (int i = 0; i < administrators.size(); i++) {

            if ((administrators.get(i).getUsername()).equals(username) && (administrators.get(i).getPassword()).equals(password)) {
                founded = true;
                name = administrators.get(i).getName();
                lastname = administrators.get(i).getLastName();
            }

        }

        return founded;
    }

    //Display a message with the name and lastname the user who has just log in
//    public void sayHi(String name, String lastname) {
//        JOptionPane.showMessageDialog(null, String.format("Bienvenido %s %s", name, lastname));
//    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == loginView.btnLogin) {
            
            login();
        }

        if (e.getSource() == loginView.btnCancel) {

            LandingView landingView = new LandingView();
            LandingControl landingControl = new LandingControl(landingView);
            landingControl.initComponents();
            loginView.setVisible(false);
        }
    }
}
