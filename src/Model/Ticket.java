package Model;

public class Ticket {
    
    private String folio;
    private String showName;
    private String seat;
    private String schedule;
    private String date;

    public Ticket(String folio, String showName, String seat, String schedule, String date) {
        
        this.folio = folio;
        this.showName = showName;
        this.seat = seat;
        this.schedule = schedule;
        this.date = date;
    }

    public String getFolio() {
        return folio;
    }

    public String getShowName() {
        return showName;
    }

    public String getSeat() {
        return seat;
    }

    public String getSchedule() {
        return schedule;
    }

    public String getDate() {
        return date;
    }
}
