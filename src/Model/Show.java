package Model;

public class Show 
{
    private int idShow;
    private String showName;
    private String description;
    private String actors;
    private int duration;
    private double cost;
    private String nameManager;
    private String phone;
    private String email;
    private boolean avaible;

    public Show() {}
    
    public Show(int idShow, String showName, String description, String actors, int duration, double cost, String nameManager, 
            String phone, String email) 
    {
        this.idShow = idShow;
        this.showName = showName;
        this.description = description;
        this.actors = actors;
        this.duration = duration;
        this.cost = cost;
        this.nameManager = nameManager;
        this.phone = phone;
        this.email = email;
    }
    
    public Show(String showName, String description, String actors, int duration, double cost, String nameManager, 
            String phone, String email) 
    {
        this.showName = showName;
        this.description = description;
        this.actors = actors;
        this.duration = duration;
        this.cost = cost;
        this.nameManager = nameManager;
        this.phone = phone;
        this.email = email;
    }

    public Show(int idShow, String showName, String description, int duration) 
    {
        this.idShow = idShow;
        this.showName = showName;
        this.description = description;
        this.duration = duration;
    }

    public int getIdShow() 
    {
        return idShow;
    }

    public void setIdShow(int idShow) 
    {
        this.idShow = idShow;
    }

    public String getShowName() 
    {
        return showName;
    }

    public void setShowName(String showName) 
    {
        this.showName = showName;
    }

    public String getDescription() 
    {
        return description;
    }

    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getActors() 
    {
        return actors;
    }

    public void setActors(String actors) 
    {
        this.actors = actors;
    }

    public int getDuration() 
    {
        return duration;
    }

    public void setDuration(int duration) 
    {
        this.duration = duration;
    }

    public double getCost() 
    {
        return cost;
    }

    public void setCost(double cost) 
    {
        this.cost = cost;
    }

    public String getNameManager() 
    {
        return nameManager;
    }

    public void setNameManager(String nameManager) 
    {
        this.nameManager = nameManager;
    }

    public String getPhone() 
    {
        return phone;
    }

    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getEmail() 
    {
        return email;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public boolean isAvaible() 
    {
        return avaible;
    }

    public void setAvaible(boolean avaible) 
    {
        this.avaible = avaible;
    }
}
