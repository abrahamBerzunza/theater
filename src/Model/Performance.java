package Model;

public class Performance {

    private int idPerformance;
    private int day;
    private int month;
    private int year;
    private int hourInitial;
    private int minuteInitial;
    private int hourFinal;
    private int minuteFinal;
    private boolean avaible;
    private int idShow;
    private String showName;

    public Performance() {}

    public Performance(int idPerformance, int day, int month, int year, int hourInitial, int minuteInitial, 
            int hourFinal, int minuteFinal, boolean avaible, int idShow) {
        

        this.idPerformance = idPerformance;
        this.day = day;
        this.month = month;
        this.year = year;
        this.hourInitial = hourInitial;
        this.minuteInitial = minuteInitial;
        this.hourFinal = hourFinal;
        this.minuteFinal = minuteFinal;
        this.avaible = avaible;
        this.idShow = idShow;
    }

    public Performance(int day, int month, int year, int hourInitial, int minuteInitial, int hourFinal, int minuteFinal) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hourInitial = hourInitial;
        this.minuteInitial = minuteInitial;
        this.hourFinal = hourFinal;
        this.minuteFinal = minuteFinal;
    }
    
    

    public Performance(int day, int month, int year, int hourInitial, int minuteInitial) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hourInitial = hourInitial;
        this.minuteInitial = minuteInitial;
    }
    
    public Performance(String showName, int day, int month, int year, int hourInitial, 
            int minuteInitial, int hourFinal, int minuteFinal) {
        
        this.showName = showName;
        this.day = day;
        this.month = month;
        this.year = year;
        this.hourInitial = hourInitial;
        this.minuteInitial = minuteInitial;
        this.hourFinal = hourFinal;
        this.minuteFinal = minuteFinal;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getHourInitial() {
        return hourInitial;
    }

    public int getMinuteInitial() {
        return minuteInitial;
    }

    public int getHourFinal() {
        return hourFinal;
    }

    public int getMinuteFinal() {
        return minuteFinal;
    }

    public boolean isAvaible() {
        return avaible;
    }

    public int getIdPerformance() {
        return idPerformance;
    }

    public int getIdShow() {
        return idShow;
    }

    public String getShowName() {
        return showName;
    }

   
    public void setIdShow(int idShow) {
        this.idShow = idShow;
    }
    

    @Override
    public String toString() {
        String hourFinal = String.format("%02d:%02d", getHourFinal(), getMinuteFinal());
        String hourInit = String.format("%02d:%02d", getHourInitial(), getMinuteInitial());
        String dateShow = day + "/" + month + "/" + year + " de " + hourInit + " a " + hourFinal;
        return dateShow;
    }

}
