package Model;

import org.apache.commons.codec.digest.DigestUtils;

public class Administrator 
{
    private String name;
    private String lastName;
    private String username;
    private String password;

    public Administrator() {}

    public Administrator(String name, String lastName, String username, String password) 
    {
        this.username = username;
        this.name = name;
        this.lastName = lastName;
        this.password = password;
    }

    public String getUsername() 
    {
        return username;
    }

    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getName() 
    {
        return name;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public String getLastName() 
    {
        return lastName;
    }

    public void setLastName(String lastName) 
    {
        this.lastName = lastName;
    }

    public String getPassword() 
    {
        return password;
    }

    public void setPassword(String password) 
    {
        this.password = password;
    }
    
    //Method for hashing password 
    public String HashPassword(String password)
    {
        String passwordHashed = null;
        
        passwordHashed = DigestUtils.md5Hex(password);
        
        return passwordHashed;
    }
}
