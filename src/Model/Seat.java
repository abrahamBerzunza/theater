package Model;

import static Controller.SeattingsControl.LIGHT_BLUE;
import Database.DAOSeat;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;

public class Seat extends JButton implements ActionListener {
    
    private int row;
    private int column;
    private String zone;
    private double basePrice;
    private double priceByZone;
    private String numberSeat;
    private int idPerformance;
    private int idClient;
    
    public Seat() {}
    
    public Seat(double basePrice, int idPerformance) {
        
        this.basePrice = basePrice;
        this.idPerformance = idPerformance;
    }

    public Seat(int row, int column, String zone, double priceByZone, int idPerformance, int idClient) {
        
        this.row = row;
        this.column = column;
        this.zone = zone;
        this.priceByZone = priceByZone;
        this.idPerformance = idPerformance;
        this.idClient = idClient;
        
        setOpaque(true);
        addActionListener(this);
    }
    
    public Seat(int row, int column, String zone, String numberSeat, double priceByZone, int idPerformance) {
        
        this.row = row;
        this.column = column;
        this.zone = zone;
        this.numberSeat = numberSeat;
        this.priceByZone = priceByZone;
        this.idPerformance = idPerformance;
    }
    
    public Seat(int row, int column, String zone, String numberSeat, double priceByZone, int idPerformance, int idClient) {
        
        this.row = row;
        this.column = column;
        this.zone = zone;
        this.numberSeat = numberSeat;
        this.priceByZone = priceByZone;
        this.idPerformance = idPerformance;
        this.idClient = idClient;
    }

    public int getRow() {
        return row;
    }
    
    public int getColumn() {
        return column;
    }

    public String getZone() {
        return zone;
    }

    public int getIdPerformance() {
        return idPerformance;
    }
    
    public double getBasePrice() {
        return basePrice;
    }

    public double getPriceByZone() {
        return priceByZone;
    }

    public void setPriceByZone(double priceByZone) {
        this.priceByZone = priceByZone;
    }

    public String getNumberSeat() {
        return numberSeat;
    }

    public int getIdClient() {
        return idClient;
    }

    //Control the seatting grid one by one
    @Override
    public void actionPerformed(ActionEvent e) {
        
        DAOSeat daoSeat = new DAOSeat();
  
        if(getBackground() == Color.RED) {
            
            daoSeat.deleteSeatByPosition(getRow(), getColumn());
            
            switch(getZone()) {
                case "Plata":
                    setBackground(LIGHT_BLUE);
                    break;
                
                case "Lata":
                    setBackground(Color.LIGHT_GRAY);
                    break;
                    
                case "Oro":
                    setBackground(Color.YELLOW);
                    break;
                    
                case "Diamante":
                    setBackground(Color.GREEN);
                    break;
                    
                case "Cobre":
                    setBackground(Color.ORANGE);
                    break;
            }
            
        }
        else {
            setBackground(Color.RED);
            
            Seat seat = new Seat(getRow(), getColumn(), getZone(), getText(), getPriceByZone(), getIdPerformance());
            daoSeat.createSeat(seat);
        }
    }
    
    
}
