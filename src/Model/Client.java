package Model;

public class Client {
    
    private int idClient;
    private String name;
    private String lastName;
    private String secondLastName;
    private double amount;
    private String date;

    public Client(String name, String lastName, String secondLastName, double amount, String date) {
        this.name = name;
        this.lastName = lastName;
        this.secondLastName = secondLastName;
        this.amount = amount;
        this.date = date;
    }

    public Client(int idClient, String name, String lastName, String secondLastName, double amount, String date) {
        this.idClient = idClient;
        this.name = name;
        this.lastName = lastName;
        this.secondLastName = secondLastName;
        this.amount = amount;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public double getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    public int getIdClient() {
        return idClient;
    }

    public String getFullName(){
        return getName() + " " + getLastName();
    }
    
    
}
