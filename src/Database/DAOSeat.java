package Database;

import Model.Seat;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class DAOSeat extends DAOGeneral {
    
    //Save seat user selection 
    public void createSeat(Seat seatting)
    {
        String query;
        
        int row = seatting.getRow();
        int column = seatting.getColumn();
        String zone = seatting.getZone();
        String numberSeat = seatting.getNumberSeat();
        double price = seatting.getPriceByZone();
        int idPerformance = seatting.getIdPerformance();

        try 
        {
            query = String.format("INSERT INTO seattings (row, col, zone, number_seat, price, id_performance) "
                    + "VALUES(%d, %d, '%s', '%s', %f, %d)", row, column, zone, numberSeat, price, idPerformance);
            
            super.transactionToDatabase(query);
        } 
        catch (SQLException ex) 
        {
            JOptionPane.showMessageDialog(null, "Error en create seat " + ex);
        }
    }
    
    //General for optimizing "getSeat and getSingleSale method"
    private ArrayList getSeat(String query)
    {
        ArrayList<Seat> salesList = new ArrayList();
        
        try 
        {
            Connection cn = enableConnection();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next())
            {   
                Seat seat = new Seat(rs.getInt("row"), rs.getInt("col"), rs.getString("zone"), rs.getString("number_seat"), 
                        rs.getDouble("price"), rs.getInt("id_performance"), rs.getInt("id_client"));
                
                salesList.add(seat);
            }
            
            st.close();
            disableConnection(cn);
        } 
        catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Error en obtener seattings " + e);
        }
        
        return salesList;
    }
    
    //Get all seatting in the Database
    public ArrayList getAllSeattings()
    {
        ArrayList<Seat> seattingsList;
        seattingsList = getSeat("SELECT * FROM seattings ORDER BY row, col");
        
        return seattingsList;
    }
    
    public ArrayList getAllSeattingsSelected() {
        
        ArrayList<Seat> seattingsList;
        seattingsList = getSeat("SELECT * FROM seattings WHERE id_client IS NULL");
        
        return seattingsList;
    }
    
    public ArrayList getAllSeattingsOccuped(int idPerformance) {
        
        ArrayList<Seat> seattingsList;
        String query;
        
        query = String.format("SELECT * FROM seattings WHERE id_performance = %d  ORDER BY zone", idPerformance);
                
        seattingsList = getSeat(query);
        
        return seattingsList;
    }
    
    public ArrayList getAllSeattingsOccupedByZone(int idPerformance, String zone) {
        
        ArrayList<Seat> seattingsList;
        String query;
        
        query = String.format("SELECT * FROM seattings WHERE id_performance = %d AND zone = '%s'  ORDER BY zone", idPerformance, zone);
                
        seattingsList = getSeat(query);
        
        return seattingsList;
    }
    
    public Seat getSeattingByNumberSeat(String numberSeat, int idPerformance) {
        
        ArrayList<Seat> seattingsList;
        String query;
        
        query = String.format("SELECT * FROM seattings WHERE id_performance = %d AND number_seat = '%s'", idPerformance, numberSeat);
                
        seattingsList = getSeat(query);
        
        return seattingsList.get(0);
    }
    
   public ArrayList getSeattingsCanceled() {
       
       ArrayList idClientsList = new ArrayList();
       String query;
       
       query = "SELECT DISTINCT(id_client) FROM seattings WHERE id_performance IS NULL";
        
       try {
            
            Connection cn = enableConnection();
            PreparedStatement statement = cn.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            
            while(result.next()) {
                
                idClientsList.add(result.getString(1));
            }
            
        } catch (SQLException ex) {
            
            System.out.println(ex.getMessage());
        }
        
       return idClientsList;
   }
    
    public void deleteSeatByNumberSeat(String numberSeat, int idPerformance) {
        
        try {
            
            String query;
            
            query = String.format("DELETE FROM seattings WHERE id_performance = %d and number_seat = '%s'", idPerformance, numberSeat);
            
            super.transactionToDatabase(query);
            
        } catch (SQLException ex) {
            
            System.out.println("error en deleteSeat " + ex);
        }
    }
    
    //delete seat switch user selection
    public void deleteSeatByPosition(int row, int column) {
        
        try {
            
            String query;
            
            query = String.format("DELETE FROM seattings WHERE row = %d AND col = %d", row, column);
            
            super.transactionToDatabase(query);
            
        } catch (SQLException ex) {
            System.out.println("error en deleteSeat " + ex);
        }
    }
    
    public void deleteSelectedSeattings() {
        
        try {
            
            String query;
            
            query = "DELETE FROM seattings WHERE id_client IS NULL";
            
            super.transactionToDatabase(query);
            
        } catch (SQLException ex) {
            
            System.out.println("error en deleteSeat " + ex);
        }
    }
    
        
    public void setClientToSeatting(int idClient) {
        
        try 
        {
            String query;
            
            query = String.format("UPDATE seattings SET id_client = %d WHERE id_client IS NULL;", idClient);
            
            super.transactionToDatabase(query);
        } 
        catch (SQLException ex) 
        {
            JOptionPane.showMessageDialog(null, "Error en setClientToSeatting " + ex);
        }
    }
    
    public void setPerformanceToSeatting(int idPerformance) {
        
        try 
        {
            String query;
            
            query = String.format("UPDATE seattings SET id_performance = %d WHERE id_performance IS NULL;", idPerformance);
            
            super.transactionToDatabase(query);
        } 
        catch (SQLException ex) 
        {
            JOptionPane.showMessageDialog(null, "Error en setPerformanceToSeatting " + ex);
        }
    }
}
