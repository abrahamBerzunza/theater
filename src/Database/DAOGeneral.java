package Database;

import Model.Performance;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class DAOGeneral 
{
    //Enable connection with database
    public Connection enableConnection() 
    {
        Connection connect = null;
        
        try 
        {
            Class.forName("org.postgresql.Driver");
            connect = DriverManager.getConnection("jdbc:postgresql://localhost:5432/theater", "postgres", "postgres");
        } 
        catch (ClassNotFoundException | SQLException ex) 
        {
            System.out.println("Error " + ex);
        }
        
        return connect;
    }
    
    //Disable connection to database
    public void disableConnection(Connection cn) 
    {
        try 
        {
            cn.close();
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error al cerrar coneccion " + ex);
        }
    }
    
    /*
        METHODS IN COMMON
    */
    
    //Mehtod for creating and edit 
    public void transactionToDatabase(String query) throws SQLException 
    {
            Connection cn = enableConnection();
          
            Statement st = cn.createStatement();
            st.executeUpdate(query);
            
            st.close();
            disableConnection(cn);
    }
    
    //Method general for getting Shows JOIN Performance
    private ArrayList<Performance> getShowsWithPerformances(String query) {
        
        
        
        ArrayList<Performance> performanceList = new ArrayList();
        
        try 
        {
            Connection cn = enableConnection();
            Statement statement = cn.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            
            while(resultSet.next())
            {
                Performance performance = new Performance(resultSet.getString("show_name"), resultSet.getInt("day"), 
                    resultSet.getInt("month"), resultSet.getInt("year"), resultSet.getInt("hour_init"), 
                    resultSet.getInt("minute_init"), resultSet.getInt("hour_final"), resultSet.getInt("minute_final"));
                
                performanceList.add(performance);
            }
            
            statement.close();
            disableConnection(cn);
        } 
        catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Error en obtener shows and performances " + e);
        }
        
        return performanceList;
    }
    
    public ArrayList<Performance> getAllShowsWithPerformances() {
        
        String query;
        ArrayList<Performance> performanceList;
        
        query = "SELECT show_name, day, month, year, hour_init, minute_init, hour_final, "
                + "minute_final FROM shows NATURAL JOIN performances ORDER BY year, month, day, hour_init, minute_init";
        
        performanceList = getShowsWithPerformances(query);
        
        return performanceList;
    }
    
    public ArrayList<Performance> getShowsWithPerformancesFiltered(int fromYear, int fromMonth, int fromDay, 
            int toYear, int toMonth, int toDay) {
        
        String query;
        
        ArrayList<Performance> performanceList;
        
        query = String.format("SELECT show_name, day, month, year, hour_init, minute_init, hour_final, "
                + "minute_final FROM shows NATURAL JOIN performances "
                + "WHERE (year BETWEEN %d AND %d) AND "
                + "(month BETWEEN %d AND %d) AND "
                + "(day BETWEEN %d AND %d) "
                + "ORDER BY year, month, day, hour_init, minute_init", fromYear, toYear, fromMonth, toMonth, fromDay, toDay);
        
        performanceList = getShowsWithPerformances(query);
        
        return performanceList;
    }
    
    //General method for executting aggregation functions in the database
    private String executeOperation(String query) {
        
        try {
            
            Connection cn = enableConnection();
            PreparedStatement statement = cn.prepareStatement(query);
            ResultSet result = statement.executeQuery();
            result.next();
            return result.getString(1);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        return null;
    }
    
    public double getTotalAmountSales() {
        
        String response;
        double amount;
        
        response = executeOperation("SELECT SUM(amount) FROM clients");
        amount = Double.valueOf(response);
        
        return amount;
    }
    
    public int getSeattingsSoldByZone(String zone) {
        
        String response, query;
        int seattings;
        
        query = String.format("SELECT COUNT(*) FROM seattings WHERE zone = '%s'", zone);
        response = executeOperation(query);
        seattings = Integer.valueOf(response);
        
        return seattings;
    }
    
    public int getTotalSeattingsSold() {
        
        String response;
        int seattings;
        
        response = executeOperation("SELECT COUNT(*) FROM seattings");
        seattings = Integer.valueOf(response);
        
        return seattings;
    }
    
}
