package Database;


import Model.Show;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class DAOShow extends DAOGeneral
{

    public void createShow(Show show)
    {
        String query;
        
        String showName = show.getShowName();
        String description = show.getDescription();
        String actors = show.getActors();
        int duration = show.getDuration();  
        double cost = show.getCost(); 
        String nameManager = show.getNameManager(); 
        String phone = show.getPhone();
        String email = show.getEmail();
        
        try 
        {
            query = String.format("INSERT INTO shows (show_name, description, actors, duration, cost, name_manager, phone, email, avaible) "
                + "VALUES ( '%s', '%s', '%s', %d, %f, '%s', '%s', '%s', '%b' )", showName, description, actors, duration, 
                cost, nameManager, phone, email, true);
            
            super.transactionToDatabase(query);
            
            JOptionPane.showMessageDialog(null, "Obra agregada con éxito");
        } 
        catch (SQLException ex) 
        {
            JOptionPane.showMessageDialog(null, "El nombre de la obra que ingresó ya existe");
        }
    }
    
    //General for optimizing "getShow and getSingleShow method"
    private ArrayList getShows(String query)
    {
        ArrayList<Show> showsList = new ArrayList();
        
        try 
        {
            Connection cn = enableConnection();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next())
            {
                Show show = new Show(rs.getInt("id_show"), rs.getString("show_name"), rs.getString("description"), 
                        rs.getString("actors"), rs.getInt("duration"), rs.getDouble("cost"), 
                        rs.getString("name_manager"), rs.getString("phone"), rs.getString("email"));
                
                showsList.add(show);
            }
            
            st.close();
            disableConnection(cn);
        } 
        catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Error en obtener shows " + e);
        }
        
        return showsList;
    }
    
    //Get all shows in the Database
    public ArrayList getAllShows()
    {
        ArrayList<Show> showsList;
        showsList = getShows("SELECT * FROM shows");
        
        return showsList;
    }
    
    public Show getSingleShowByName(String showName)
    {
        ArrayList<Show> show;
        String query; 
        
        query = String.format("SELECT * FROM shows WHERE show_name = '%s'", showName);
        show = getShows(query);
        
        return show.get(0);
    }
    

    public Show getSingleShowById(int idShow) {
        ArrayList<Show> show;
        String query; 
        
        query = String.format("SELECT * FROM shows WHERE id_show = %d", idShow);

        show = getShows(query);
        
        return show.get(0);
    }

    public void deleteShowById(int idShow) {
        try {
            
            String query;
            
            query = String.format("DELETE FROM shows WHERE id_show = %d", idShow);
            
            super.transactionToDatabase(query);
            
        } catch (SQLException ex) {
            System.out.println("error en setNoAvailableShow " + ex);
        }
    }

    public void updateShowById(Show show, int idShow) {
        
        String query;
        
        String showName = show.getShowName();
        String description = show.getDescription();
        String actors = show.getActors();
        int duration = show.getDuration();  
        double cost = show.getCost(); 
        String nameManager = show.getNameManager(); 
        String phone = show.getPhone();
        String email = show.getEmail();
        
        try 
        {
            query = String.format("UPDATE shows SET show_name = '%s', description = '%s', actors = '%s', duration = %d, cost = %f, "
                    + "name_manager = '%s', phone = '%s', email = '%s' WHERE id_show = %d", showName, description, actors, duration, 
                                                                            cost, nameManager, phone, email, idShow);
            
            super.transactionToDatabase(query);
            
            JOptionPane.showMessageDialog(null, "Obra actualizada con éxito");
        } 
        catch (SQLException ex) 
        {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "El nombre de la obra que ingresó ya existe");
        }
    }

   
}
