package Database;

import Model.Administrator;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class DAOAdministrator extends DAOGeneral
{
    public void createAdministrator(String name, String lastName, String username, String password)
    {
        String query;
        
        try 
        {
            query = String.format("INSERT INTO administrator (name, last_name, username, password) "
                + "VALUES ('%s', '%s', '%s', '%s')", name, lastName, username, password);
            
            transactionToDatabase(query);
        } 
        catch (SQLException ex) 
        {
            System.out.println("Error en createAdministrator" + ex);
        }
    }
    
    //Method for getting administrator from database
    public ArrayList getAdministrators() 
    {
        ArrayList<Administrator> administratorsList = new ArrayList();
       
        try 
        {
            Connection cn = enableConnection();

            String query = "SELECT * FROM administrator";
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next())
            {
                Administrator administrator = new Administrator(rs.getString("name"), rs.getString("last_name"), 
                        rs.getString("username"), rs.getString("password"));
                
                administratorsList.add(administrator);
            }
            
            st.close();
            disableConnection(cn);
        } 
        catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Error en obtener administradores " + e);
        }
        
        return administratorsList;
    }

}
