package Database;

import Model.Client;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class DAOClient extends DAOGeneral {
    
    public void createClient(Client client)
    {
        String query;
        
        String name = client.getName();
        String lastName = client.getLastName();
        String secondLastName = client.getSecondLastName();
        double amount = client.getAmount();
        String date = String.valueOf(client.getDate());
        
        try 
        {
            query = String.format("INSERT INTO clients (name, last_name, second_last_name, amount, date) "
                    + "VALUES('%s', '%s', '%s', %f, '%s')", name, lastName, secondLastName, amount, date);
            
            super.transactionToDatabase(query);
            
            JOptionPane.showMessageDialog(null, "Compra realizada con éxito");
        } 
        catch (SQLException ex) 
        {
            JOptionPane.showMessageDialog(null, "error en create client " + ex);
        }
    }
    
    //General for optimizing getClientIdByName
    private ArrayList getClient(String query)
    {
        ArrayList<Client> clientList = new ArrayList();
        
        try 
        {
            Connection cn = enableConnection();
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(query);
            
            while(rs.next())
            {
                
                Client client = new Client(rs.getInt("id_client"), rs.getString("name"), rs.getString("last_name"), 
                        rs.getString("second_last_name"), rs.getDouble("amount"), rs.getString("date"));
                
                clientList.add(client);
            }
            
            st.close();
            disableConnection(cn);
        } 
        catch (SQLException e) 
        {
            JOptionPane.showMessageDialog(null, "Error en obtener clients " + e);
        }
        
        return clientList;
    }
    
    public ArrayList<Client> getAllClients() {
        
        ArrayList<Client> clientList;
        
        clientList = getClient("SELECT * FROM clients ORDER BY last_name");
        
        return clientList;
    }
    
    public int getClientIdByName(String name, String lastName, String secondLastName) {
        
        ArrayList<Client> client;
        String query;
        
        query = String.format("SELECT * FROM clients WHERE name = '%s' AND last_name = '%s' AND second_last_name = '%s'", 
                name, lastName, secondLastName);
        
        client = getClient(query);
        
        return client.get(0).getIdClient();
    }
    
    public Client getClientById(int idClient) {
        
        ArrayList<Client> client;
        String query;
        
        query = String.format("SELECT * FROM clients WHERE id_client = %d", idClient);
        
        client = getClient(query);
        
        return client.get(0);
    }
    
    public void updateAmount(double amount, int idClient) {
        
        try 
        {
            String query;
            
            query = String.format("UPDATE clients SET amount = %f WHERE id_client = %d;", amount, idClient);
            
            super.transactionToDatabase(query);
        } 
        catch (SQLException ex) 
        {
            JOptionPane.showMessageDialog(null, "Error en updateAmount() " + ex);
        }
    }
    
    public void deleteClientById(int idClient) {
        
        try {
            
            String query;
            
            query = String.format("DELETE FROM clients WHERE id_client = %d", idClient);
            
            super.transactionToDatabase(query);
            
        } catch (SQLException ex) {
            
            System.out.println("error en deleteSeat " + ex);
        }
    }
    
}
