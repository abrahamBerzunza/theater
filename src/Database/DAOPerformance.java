package Database;

import Model.Performance;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class DAOPerformance extends DAOGeneral {

    public int createPerformance(int day, int month, int year, int hourInit, int minuteInit, int hourFinal,
            int minuteFinal, int idShow) {
        
        String query;

        try {
            if (isValidPerformance(day, month, year, hourInit, hourFinal)) {
                query = String.format("INSERT INTO performances (day, month, year, hour_init, minute_init, hour_final, "
                        + "minute_final, avaible, id_show) "
                        + "VALUES(%d, %d, %d, %d, %d, %d, %d, '%b', %d)", day, month, year, hourInit, minuteInit,
                        hourFinal, minuteFinal, true, idShow);

                super.transactionToDatabase(query);

                return 0;
            } 
            else {
                
                JOptionPane.showMessageDialog(null, "El horario que ingresó se encuentra ocupado");
            }

        } 
        catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "La función que ingresó ya ha sido asignada");
        }  
        
        return 1;
    }

    //General for optimizing other transactions for getting performances  
    private ArrayList getPerformance(String query) {
        ArrayList<Performance> performanceList = new ArrayList();

        try {
            Connection connection = enableConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                Performance performance = new Performance(resultSet.getInt("id_performance"), resultSet.getInt("day"), 
                        resultSet.getInt("month"), resultSet.getInt("year"), resultSet.getInt("hour_init"), 
                        resultSet.getInt("minute_init"), resultSet.getInt("hour_final"),
                        resultSet.getInt("minute_final"), resultSet.getBoolean("avaible"), resultSet.getInt("id_show"));

                performanceList.add(performance);
            }

            statement.close();
            disableConnection(connection);
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Error en obtener las funciones " + e);
        }

        return performanceList;
    }
    
   
    public ArrayList getAllPerformances() {
        ArrayList<Performance> performanceList;
        String query;

        query = "SELECT * FROM performances ORDER BY year, month, day, hour_init, minute_init";

        performanceList = getPerformance(query);

        return performanceList;
    }

    public ArrayList getPerformancesByShowId(int idShow) {
        ArrayList<Performance> performance;
        String query;

        query = String.format("SELECT * FROM performances WHERE id_show = %d ORDER BY year, month, day, hour_init, minute_init ", idShow);
        performance = getPerformance(query);

        return performance;
    }
    
    public Performance getPerformanceById(int idPerformance) {
        ArrayList<Performance> performance;
        String query;

        query = String.format("SELECT * FROM performances WHERE id_performance = %d", idPerformance);
        performance = getPerformance(query);

        return performance.get(0);
    }
    
    
    public Performance getPerformanceByDate(int day, int month, int year, int hourInit, int minuteInit) {
        
        ArrayList<Performance> performance;
        String query;
        
        query = String.format("SELECT * FROM performances WHERE  day = '%d' AND month = '%d' "
                + "AND year = '%d' AND hour_init = '%d' AND minute_init = '%d'", day, month, year, hourInit, minuteInit);
        performance = getPerformance(query);
        
        return performance.get(0);
    }
    
    public void deletePerformance(int day, int month, int year, int hourInit, int minuteInit) {
            
        try {
            
            String query;
            
            query = String.format("DELETE FROM performances WHERE  day = '%d' AND month = '%d' "
                    + "AND year = '%d' AND hour_init = '%d' AND minute_init = '%d'",
                    day, month, year, hourInit, minuteInit);
            
            super.transactionToDatabase(query);
            
        } catch (SQLException ex) {
            System.out.println("error en deletePerformance " + ex);
        }
    }

    /*
     *  Validations performances
     */
    public boolean isValidPerformance(int day, int month, int year, int hourInit, int hourFinal) {
        
        ArrayList<Performance> performances;
        boolean isTheSameDay, isTheSameMonth, isTheSameYear;
        boolean inRangeHourInit, inRangeHourFinal;

        performances = getAllPerformances();

        for (int i = 0; i < performances.size(); i++) {
            isTheSameDay = performances.get(i).getDay() == day;
            isTheSameMonth = performances.get(i).getMonth() == month;
            isTheSameYear = performances.get(i).getYear() == year;

            inRangeHourInit = (hourInit >= performances.get(i).getHourInitial()) && (hourInit <= performances.get(i).getHourFinal());
            inRangeHourFinal = (hourFinal >= performances.get(i).getHourInitial()) && (hourFinal <= performances.get(i).getHourFinal());

            if (isTheSameDay && isTheSameMonth && isTheSameYear) {
                if (inRangeHourInit) {
                    return false;
                }

                if (inRangeHourFinal) {
                    return false;
                }
            }

        }

        return true;
    }
}
